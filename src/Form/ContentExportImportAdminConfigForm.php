<?php

namespace Drupal\content_export_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\node\Entity\NodeType;



class ContentExportImportAdminConfigForm extends FormBase {
	/**
	 *  function used to put form unique ID.
	 */
	public function getFormId() {
		return 'content_export_import_admin_config_form';
	}
	
	
	/**
	 *  buildForm() used for creating a form
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {
		$all_content_types = NodeType::loadMultiple();
		/** @var NodeType $content_type */
		foreach ($all_content_types as $machine_name => $content_type) {
			$content_type_name[$content_type->id()] = $content_type->label();
		}
		$form['content_type_list'] = array(
				'#type' => 'select',
				'#title' => $this->t('Select Content Type'),
				'#multiple' => TRUE,
				'#required' => TRUE ,
				'#options' => $content_type_name,
		);
		$form['file_format'] = array(
				'#type' => 'select',
				'#title' => $this->t('Select Format'),
				'#required' => TRUE ,
				'#options' => array('csv' => $this->t('CSV'), 'xml' => $this->t('XML'), 'json' => $this->t('JSON')),
		);
		$form['select_operation'] = array(
				'#type' => 'checkbox',
				'#title' => $this->t('Select Operations'),
				'#required' => TRUE,
		);
		$form['operation_fieldset'] = array(
				'#type' => 'fieldset',
				'#states' => array(
						'visible' => array(
								':input[name="select_operation"]' => array('checked' => TRUE),
						),
				),
		);
		$form['operation_fieldset']['operation_list'] = array(
				'#type' => 'radios',
				'#required' => TRUE ,
				'#options' => array('import' => $this->t('Import'), 'export' => $this->t('Export'), 'download_template' => $this->t('Download Template')),
				'#ajax' => array(
						'callback' => [$this, '_update_submit_value'],
						'wrapper' => 'submit-field',
						'effect' => 'fade',
				),
		);
		$form['file'] = array(
				'#title' => $this->t('Upload File'),
				'#description' => $this->t('Upload a file from your computer'),
				'#type' => 'file',
				'#states' => array(
						'visible' => array(
								':input[name="operation_list"]' => array('value' => 'import'),
						),
				),
		);
		$operation_data = $form_state->getValue('operation_list');
		$form['submit'] = array(
				'#type' => 'submit',
				'#prefix' => '<div id="submit-field">',
				'#suffix' => '</div>',
				'#value' => isset($operation_data)? $operation_data : 'Submit',
		);
		return $form;
	}
	/**
	 * Ajax Callback
	 */
	public function _update_submit_value(array $form, FormStateInterface $form_state){
		return $form['submit'];
	}
	/**
	 * Submit Function
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
		/*  Put data according to user need   */
	}
}